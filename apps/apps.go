package apps

import (
	_ "gitlab.com/ops5751001/vblog/apps/blog/api"
	_ "gitlab.com/ops5751001/vblog/apps/blog/impl"
	_ "gitlab.com/ops5751001/vblog/apps/token/api"
	_ "gitlab.com/ops5751001/vblog/apps/token/impl"
	_ "gitlab.com/ops5751001/vblog/apps/user/impl"
)
