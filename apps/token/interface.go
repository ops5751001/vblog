package token

import "context"

type Service interface {
	// 登录: 颁发令牌
	Issue(context.Context, *IssueRequest) (*Token, error)
	// 退出: 撤销令牌
	Revoke(context.Context, *RevokeRequest) (*Token, error)
	// 校验令牌
	Validate(context.Context, *ValidateRequest) (*Token, error)
	// // 清理过期令牌
	// Clen(context.Context, *ValidateRequest) (*Token, error)
}
