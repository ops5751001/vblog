package impl

import (
	"context"
	"fmt"

	"gitlab.com/ops5751001/vblog/apps/token"
	"gitlab.com/ops5751001/vblog/apps/user"
	conf "gitlab.com/ops5751001/vblog/config"
	"gitlab.com/ops5751001/vblog/ioc"
	"gorm.io/gorm"
)

// 强制检查约束
var _ token.Service = (*TokenImpl)(nil)
var _ ioc.Object = (*TokenImpl)(nil)

func init() {
	ioc.Controller().Register(token.APPNAME, &TokenImpl{})
}

func NewTokenImpl(u user.Service) *TokenImpl {
	return &TokenImpl{
		db:   conf.GetDB(),
		user: u,
	}
}

type TokenImpl struct {
	db   *gorm.DB
	user user.Service
}

// 颁发Token
func (t *TokenImpl) Issue(ctx context.Context, req *token.IssueRequest) (*token.Token, error) {
	// 1. 校验请求
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 校验并查询Password
	us, err := t.user.CheckUserPassword(ctx, user.NewCheckRequest(req.Username, req.Password))
	if err != nil {
		return nil, err
	}
	// 3. 创建token实例
	tk := token.NewToken(us.ID, req)
	if err := t.db.WithContext(ctx).Model(&token.Token{}).Create(&tk).Error; err != nil {
		return nil, err
	}

	return tk, nil
}

// 销毁token
func (t *TokenImpl) Revoke(ctx context.Context, req *token.RevokeRequest) (*token.Token, error) {
	// 1. 校验请求
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 查找token
	var tk *token.Token
	if err := t.db.WithContext(ctx).Model(&token.Token{}).
		Where("access_token=? and refresh_token=?", req.AccessToken, req.RefreshToken).First(&tk).Error; err != nil {
		return nil, err
	}
	// 3. 删除token
	// TODO：并发读写问题
	if err := t.db.WithContext(ctx).Model(&token.Token{}).
		Where("access_token=? and refresh_token=?", req.AccessToken, req.RefreshToken).Delete(&tk).Error; err != nil {
		return nil, err
	}
	return tk, nil
}

// 校验token
func (t *TokenImpl) Validate(ctx context.Context, req *token.ValidateRequest) (*token.Token, error) {
	// 1. 校验请求
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 查找token
	var tk *token.Token
	if err := t.db.WithContext(ctx).Model(&token.Token{}).
		Where("access_token=? and refresh_token=?", req.AccessToken, req.RefreshToken).First(&tk).Error; err != nil {
		return nil, err
	}
	// 3. 校验token是否过期
	akExpired, rkExpired := tk.CheckAKExpired()
	// 分三种情况:
	// access token,refresh token 两个都过期,
	// access token 过期, refresh token没有过期，则更新数据表，
	// access token,refresh token 两个都没过期,
	if akExpired && rkExpired {
		return nil, fmt.Errorf("access token and refresh token is all expired")
	}

	if akExpired && !rkExpired {
		if err := t.db.WithContext(ctx).Model(&token.Token{}).Where("access_token=?", req.AccessToken).Save(&tk).Error; err != nil {
			return nil, err
		}
		return tk, nil
	}
	return tk, nil
}

// 向Ioc注册使用，初始化对象
func (t *TokenImpl) Init() error {
	usvc := ioc.Controller().GetObj(user.APPNAME).(user.Service)
	t.db = conf.GetDB()
	t.user = usvc
	return nil
}

// 向Ioc注册使用, 从ioc中销毁对象
func (t *TokenImpl) Destory() error {
	return nil
}
