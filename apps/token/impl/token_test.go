package impl_test

import (
	"context"
	"testing"

	"gitlab.com/ops5751001/vblog/apps/token"
	token_impl "gitlab.com/ops5751001/vblog/apps/token/impl"
	user_impl "gitlab.com/ops5751001/vblog/apps/user/impl"
	conf "gitlab.com/ops5751001/vblog/config"
)

var tki *token_impl.TokenImpl
var ctx context.Context

func init() {
	conf.ReadConfigFromEnv()
	conf.Init()
	tki = token_impl.NewTokenImpl(user_impl.NewUserServiceImpl())
}

func TestIssue(t *testing.T) {
	req := token.NewIssueRequest("zhangsan", "abcdefg", true)
	tk, err := tki.Issue(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestRevoke(t *testing.T) {
	req := token.NewRevokeRequest("cmuha3jrg3is46qetl90", "cmuha3jrg3is46qetl9g")
	tk, err := tki.Revoke(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestValidate(t *testing.T) {
	req := token.NewValidateRequest("18ahB2cuPRR3pd1g5sKXKg26k0y9xwIM", "ACMTVxW1Y6pqnXYLh4egZVjnuZJd1KNO")
	tk, err := tki.Validate(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}
