package token

import (
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
)

var validate = validator.New()

// /////////////////////////////////////////////////////
// 颁发令牌
func NewIssueRequest(username, password string, remember bool) *IssueRequest {
	return &IssueRequest{
		Username: username,
		Password: password,
		Remember: remember,
	}
}

type IssueRequest struct {
	Username string `json:"username" validate:"required,alpha"`
	Password string `json:"password,omitempty" validate:"required,ascii"`
	Remember bool   `json:"remember" validate:"-"`
}

func (i *IssueRequest) String() string {
	b, _ := json.Marshal(i)
	return string(b)
}

func (i *IssueRequest) Validate() error {
	err := validate.Struct(i)
	if err != nil {
		logrus.Warnf("valiate IssueRequest struct failed,obj:%v,err:%s", i, err)
	}
	return err
}

// /////////////////////////////////////////////////////
// 撤销令牌
func NewRevokeRequest(ak, rk string) *RevokeRequest {
	return &RevokeRequest{
		AccessToken:  ak,
		RefreshToken: rk,
	}
}

func NewRevokeRequestFromHttp(h *http.Request) (*RevokeRequest, error) {
	c, err := h.Cookie(COOKIE_ACCESS_KEY)
	if err != nil {
		return nil, err
	}
	c2, err2 := h.Cookie(COOKIE_REFRESH_KEY)
	if err2 != nil {
		return nil, err2
	}
	return NewRevokeRequest(c.Value, c2.Value), nil

}

type RevokeRequest struct {
	AccessToken  string `json:"access_token" validate:"required,ascii"`
	RefreshToken string `json:"refresh_token" validate:"required,ascii"`
}

func (r *RevokeRequest) Validate() error {
	err := validate.Struct(r)
	if err != nil {
		logrus.Warnf("valiate IssueRequest struct failed,obj:%v,err:%s", r, err)
	}
	return err
}

// /////////////////////////////////////////////////////
// 校验令牌
func NewValidateRequest(ak, rk string) *ValidateRequest {
	return &ValidateRequest{
		AccessToken:  ak,
		RefreshToken: rk,
	}
}

type ValidateRequest struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func (v *ValidateRequest) Validate() error {
	err := validate.Struct(v)
	if err != nil {
		logrus.Warnf("valiate IssueRequest struct failed,obj:%v,err:%s", v, err)
	}
	return err
}
