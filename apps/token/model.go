package token

import (
	"encoding/json"
	"math/rand"
	"time"

	conf "gitlab.com/ops5751001/vblog/config"
)

var chars = []byte("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func generateRandString(length int) string {
	l := len(chars)
	randomBytes := make([]byte, length)
	for i := range randomBytes {
		randomBytes[i] = chars[rand.Intn(l)]
	}
	return string(randomBytes)
}

func NewToken(userID int, req *IssueRequest) *Token {
	tk := &Token{
		UserID:                userID,
		Username:              req.Username,
		AccessToken:           generateRandString(32),
		RefreshToken:          generateRandString(32),
		AccessTokenExpiredAt:  conf.GetAccessTokenExpiredAt(),
		RefreshTokenExpiredAt: conf.GetRefreshTokenExpiredAt(),
		CreatedAt:             time.Now().Unix(),
	}
	if req.Remember {
		tk.RefreshTokenExpiredAt = conf.GetRefreshTokenExpiredAtWithRemeber()
	}
	return tk
}

type Token struct {
	UserID                int    `json:"user_id" gorm:"column:user_id;type:bigint;not null"`
	Username              string `json:"username" gorm:"column:username;type:char(64) not null;index"`
	AccessToken           string `json:"access_token" gorm:"column:access_token;type:char(255) primary key"`
	RefreshToken          string `json:"refresh_token" gorm:"column:refresh_token;type:char(255) not null"`
	AccessTokenExpiredAt  int64  `json:"access_token_expired_at" gorm:"column:access_token_expired_at;type:int;not null"`
	RefreshTokenExpiredAt int64  `json:"refresh_token_expired_at" gorm:"column:refresh_token_expired_at;type:int;not null"`
	CreatedAt             int64  `json:"created_at" gorm:"autoCreateTime"`
	UpdatedAt             int64  `json:"updated_at" gorm:"autoUpdateTime"`
}

func (t *Token) String() string {
	b, _ := json.Marshal(t)
	return string(b)
}

// 判断AccessToken和RefreshToken过期情况
// 如果AccessToken过期，但是Refresh未过期，则自动刷新Token
// 因为在conf中已经强制实现了 RefreshTokenExipredAt >= AccessTokenExpiredAt
func (t *Token) CheckAKExpired() (bool, bool) {
	now := time.Now().Unix()
	if t.AccessTokenExpiredAt+t.UpdatedAt > now {
		return false, false
	}
	// accessToken 过期，但是RereshToken未过期
	if t.RefreshTokenExpiredAt+t.CreatedAt > now {
		t.refresh(now)
		return true, false
	}
	return true, true
}

func (t *Token) refresh(now int64) {
	t.AccessToken = generateRandString(32)
	if t.AccessTokenExpiredAt+now > t.RefreshTokenExpiredAt+t.CreatedAt {
		t.AccessTokenExpiredAt = (t.RefreshTokenExpiredAt + t.CreatedAt) - now
	}
}
