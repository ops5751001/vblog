package api

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/ops5751001/vblog/apps/token"
	conf "gitlab.com/ops5751001/vblog/config"
	"gitlab.com/ops5751001/vblog/ioc"
	"gitlab.com/ops5751001/vblog/response"
)

// 需要实现以下接口
var _ ioc.Object = (*TokenApiHandler)(nil)
var _ ioc.GinRouter = (*TokenApiHandler)(nil)

func init() {
	ioc.ApiHandler().Register(token.APPNAME, &TokenApiHandler{})
}

func NewTokenApiHandler(svc token.Service) *TokenApiHandler {
	return &TokenApiHandler{
		svc: svc,
	}
}

type TokenApiHandler struct {
	svc token.Service
}

// 注册Gin路由
func (t *TokenApiHandler) RegisterGin(r gin.IRouter) {
	rg := r.Group(token.APPNAME)
	rg.POST("/", t.Login)
	rg.DELETE("/", t.Logout)
}

// 登录
func (t *TokenApiHandler) Login(c *gin.Context) {
	// 1. 解析请求
	var req *token.IssueRequest
	if err := c.BindJSON(&req); err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		body, _ := io.ReadAll(c.Request.Body)
		logrus.Warnf("bind json failed,obj:%v,err:%s", body, err)
		return
	}
	// 2. 处理业务逻辑
	tk, err := t.svc.Issue(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4012, err.Error())
		logrus.Warnf("issue token failed,obj:%v,err:%s", req, err)
		return
	}
	// 3. 返回结果
	// 设置返回的上下文，将ak和rk同时塞进去
	c.SetCookie(token.COOKIE_ACCESS_KEY, tk.AccessToken, int(tk.AccessTokenExpiredAt), conf.GetCookiesPath(),
		conf.GetDomainName(), false, true)
	c.SetCookie(token.COOKIE_REFRESH_KEY, tk.RefreshToken, int(tk.RefreshTokenExpiredAt), conf.GetCookiesPath(),
		conf.GetDomainName(), false, true)
	response.SendOK(c, http.StatusOK, 2000, tk)
}

// 退出登录
func (t *TokenApiHandler) Logout(c *gin.Context) {
	// 1. 从Cookies读取请求
	req, err := token.NewRevokeRequestFromHttp(c.Request)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4013, err.Error())
		logrus.Warnf("read cookies from request failed,err:%s", err.Error())
		return
	}
	// 2. 处理业务逻辑
	tk, err2 := t.svc.Revoke(c, req)
	if err2 != nil {
		response.SendFailed(c, http.StatusBadRequest, 4014, err2.Error())
		logrus.Warnf("reovke token failed,err:%s", err.Error())
		return
	}
	// 3. 返回结果
	c.SetCookie(token.COOKIE_ACCESS_KEY, "", -1, conf.GetCookiesPath(), conf.GetDomainName(), false, true)
	c.SetCookie(token.COOKIE_REFRESH_KEY, "", -1, conf.GetCookiesPath(), conf.GetDomainName(), false, true)
	response.SendOK(c, http.StatusOK, 2000, tk)
}

// 向Ioc注册使用，初始化对象
func (t *TokenApiHandler) Init() error {
	t.svc = ioc.Controller().GetObj(token.APPNAME).(token.Service)
	return nil
}

// 向Ioc注册使用, 从ioc中销毁对象
func (t *TokenApiHandler) Destory() error {
	return nil
}
