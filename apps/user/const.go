package user

type Role int8

const (
	ROLE_MEMBER Role = 0
	ROLE_ADMIN  Role = 1
	APPNAME          = "user"
)

// 方便打印
func (r Role) String() string {
	switch r {
	case 0:
		return "游客"
	case 1:
		return "管理员"
	default:
		return "Unknow"
	}
}

