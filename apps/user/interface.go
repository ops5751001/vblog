package user

import (
	"context"
)

type Service interface {
	CreateUser(ctx context.Context, req *CreateRequest) (*User, error)
	QueryUsers(ctx context.Context, req *QueryRequest) (*Set, error)
	DescribeUser(ctx context.Context, req *DescribeRequest) (*User, error)
	UpdateUser(ctx context.Context, req *UpdateRequest) (*User, error)
	DeleteUser(ctx context.Context, req *DeleteRequest) (*User, error)
	CheckUserPassword(ctx context.Context, req *CheckRequest) (*User, error)
}
