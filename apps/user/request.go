package user

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/liushuochen/gotable"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

var validate = validator.New()

// ///////////////////////////////////////////////////////////
// 创建
func NewCreateRequest() *CreateRequest {
	// 用来避免零值Map不可用问题
	return &CreateRequest{
		Label: map[string]string{},
	}
}

// 创建用户的请求
type CreateRequest struct {
	Username string            `validate:"required,alpha" gorm:"column:username;type:char(64);index" json:"username"`
	Password string            `validate:"required,ascii" gorm:"column:password;type:varchar;size:128" json:"password,omitempty"`
	Role     Role              `validate:"oneof=0 1" gorm:"column:role;tinyint" json:"role"`
	Label    map[string]string `validate:"omitempty" gorm:"serializer:json" json:"label"`
}

func (c *CreateRequest) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}

// 对用户密码加盐哈希
func (c *CreateRequest) HashPassword() error {
	b, err := bcrypt.GenerateFromPassword([]byte(c.Password), bcrypt.DefaultCost)
	if err != nil {
		logrus.Errorf("bcrypt password failed,password=%s,err:%s", c.Password, err.Error())
		return err
	}
	c.Password = string(b)
	return nil
}

// 校验请求字段
func (c *CreateRequest) Validate() error {
	if err := validate.Struct(c); err != nil {
		logrus.Warnf("valiate CreateRequest struct failed,obj:%v,err:%s", c, err)
		return err
	}
	return nil
}

// ///////////////////////////////////////////////////////////
// 查询列表
func NewQueryRequest(username string) *QueryRequest {
	return &QueryRequest{
		Username: username,
	}
}

// TODO: 这个查询功能需要丰富下，否则使用集合意义不大
type QueryRequest struct {
	Username string `json:"username"`
	PageSize int    `validate:"omitempty,gte=1" json:"page_size"`
	PageNum  int    `validate:"omitempty,gte=1" json:"page_num"`
}

func (q *QueryRequest) Validate() error {
	switch {
	case q.PageSize <= 0:
		q.PageSize = 10
	case q.PageSize > 100:
		q.PageSize = 100
	}
	if q.PageNum <= 0 {
		q.PageNum = 1
	}
	if err := validate.Struct(q); err != nil {
		logrus.Warnf("valiate struct failed, err:%s", err.Error())
		return err
	}
	return nil
}

func (q *QueryRequest) Limit() int {
	return q.PageSize
}

func (q *QueryRequest) Offset() int {
	return (q.PageNum - 1) * q.PageSize
}

func NewSet() *Set {
	return &Set{
		Items: []*User{},
	}
}

type Set struct {
	Count int     `json:"count"`
	Items []*User `json:"items"`
}

func (s *Set) String() string {
	b, _ := json.Marshal(s)
	return string(b)
}

func (s *Set) Print() {
	tb, _ := gotable.Create("no", "id", "username", "role", "created", "updated")
	var m = make(map[string]string, 20)
	for i, u := range s.Items {
		m = map[string]string{
			"no":       strconv.Itoa(i + 1),
			"id":       strconv.Itoa(u.ID),
			"username": u.Username,
			"role":     u.Role.String(),
			"created":  time.Unix(u.CreatedAt, 0).Format("2006-01-02 15:04:05 -0700"),
			"updated":  time.Unix(u.UpdatedAt, 0).Format("2006-01-02 15:04:05 -0700"),
		}
		tb.AddRow(m)
	}
	tb.OpenBorder()
	fmt.Println(tb.String())
}

/////////////////////////////////////////////////////////////
// 查询详情

func NewDescribeRequest(id int) *DescribeRequest {
	return &DescribeRequest{
		ID: id,
	}
}

type DescribeRequest struct {
	ID int `json:"id" validate:"required,gte=1"`
}

func (d *DescribeRequest) Validate() error {
	err := validate.Struct(d)
	if err != nil {
		logrus.Warnf("valiate struct failed, err:%s", err.Error())
	}
	return err
}

// ///////////////////////////////////////////////////////////
// 更新
func NewUpdateRequest(id int) *UpdateRequest {
	return &UpdateRequest{
		ID: id,
	}
}

type UpdateRequest struct {
	ID       int               `validate:"required,gte=1" json:"id"`
	Password string            `validate:"omitempty,ascii" json:"password"`
	Role     Role              `validate:"oneof=0 1" json:"role"`
	Label    map[string]string `validate:"omitempty" json:"label"`
}

func (u *UpdateRequest) Validate() error {
	err := validate.Struct(u)
	if err != nil {
		logrus.Warnf("valiate struct failed, err:%s", err.Error())
	}
	return err
}

// //////////////////////////////////////
// 删除
func NewDeleteRequest(id int) *DeleteRequest {
	return &DeleteRequest{
		ID: id,
	}
}

type DeleteRequest struct {
	ID int `validate:"required,gte=1" json:"id"`
}

func (d *DeleteRequest) Validate() error {
	err := validate.Struct(d)
	if err != nil {
		logrus.Warnf("valiate struct failed, err:%s", err.Error())
	}
	return err
}

// /////////////////////////////////////
// 校验用户密码
func NewCheckRequest(username, password string) *CheckRequest {
	return &CheckRequest{
		Username: username,
		Password: password,
	}
}

type CheckRequest struct {
	Username string `validate:"required,alpha" json:"username"`
	Password string `validate:"required,ascii" json:"password"`
}

func (c *CheckRequest) Validate() error {
	err := validate.Struct(c)
	if err != nil {
		logrus.Warnf("valiate struct failed, err:%s", err.Error())
	}
	return err
}

func (c *CheckRequest) DiffPassword(hashPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(c.Password))
}
