package impl_test

import (
	"context"
	"testing"

	"gitlab.com/ops5751001/vblog/apps/user"
	"gitlab.com/ops5751001/vblog/apps/user/impl"
	conf "gitlab.com/ops5751001/vblog/config"
)

var (
	i   *impl.UserServiceImpl
	ctx = context.Background()
)

func init() {
	conf.ReadConfigFromEnv()
	conf.Init()
	i = impl.NewUserServiceImpl()
}

func TestCreateUser(t *testing.T) {
	req := user.NewCreateRequest()
	req.Username = "zhangsan"
	req.Password = "123456"
	req.Label = map[string]string{}
	req.Role = user.ROLE_MEMBER
	u, err := i.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u)
}

func TestQueryUsers(t *testing.T) {
	req := user.NewQueryRequest("jerry")
	set, err := i.QueryUsers(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestDescribeUser(t *testing.T) {
	req := user.NewDescribeRequest(5)
	u, err := i.DescribeUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u)
}

func TestUpdateUser(t *testing.T) {
	req := user.NewUpdateRequest(5)
	req.Password = "abcdefg"
	req.Role = 0

	u, err := i.UpdateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u)
}

func TestDeleteUser(t *testing.T) {
	req := user.NewDeleteRequest(10)
	u, err := i.DeleteUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(u)
}
