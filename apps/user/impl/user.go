package impl

import (
	"context"

	"gitlab.com/ops5751001/vblog/apps/user"
	conf "gitlab.com/ops5751001/vblog/config"
	"gitlab.com/ops5751001/vblog/ioc"
	"gorm.io/gorm"
)

// 强制约束
var _ user.Service = (*UserServiceImpl)(nil)
var _ ioc.Object = (*UserServiceImpl)(nil)

func init() {
	ioc.Controller().Register(user.APPNAME, &UserServiceImpl{})
}

type UserServiceImpl struct {
	db *gorm.DB
}

func NewUserServiceImpl() *UserServiceImpl {
	return &UserServiceImpl{
		db: conf.GetDB(),
	}
}

func (u *UserServiceImpl) CreateUser(ctx context.Context, req *user.CreateRequest) (*user.User, error) {
	// 1. 检验req是否符合约束
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 将req字段密码哈希后转为User结构体实例
	if err := req.HashPassword(); err != nil {
		return nil, err
	}
	us := user.NewUser(req)
	// 3. 将user实例存入数据库
	if err := u.db.WithContext(ctx).Model(&user.User{}).Create(&us).Error; err != nil {
		return nil, err
	}
	// 4. 将ORM返回的数据中结果返回
	us.Password = ""
	return us, nil
}

func (u *UserServiceImpl) QueryUsers(ctx context.Context, req *user.QueryRequest) (*user.Set, error) {
	// 1. 校验请求合法性
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 从数据库查询
	set := user.NewSet()
	//
	condition := u.db.WithContext(ctx).Model(&user.User{})
	if req.Username != "" {
		condition = condition.Where("username=?", req.Username)
	}
	var count int64
	if err := condition.Count(&count).Error; err != nil {
		return nil, err
	}
	set.Count = int(count)

	if err := condition.Offset(req.Offset()).Limit(req.Limit()).Find(&set.Items).Error; err != nil {
		return nil, err
	}
	// 3. 不对外展示密码
	for _, us := range set.Items {
		us.Password = ""
	}

	return set, nil
}

func (u *UserServiceImpl) DescribeUser(ctx context.Context, req *user.DescribeRequest) (*user.User, error) {
	// 1. 校验请求
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 从数据库查询
	var us = user.NewUser(nil)
	if err := u.db.WithContext(ctx).Model(&user.User{}).Where("id=?", req.ID).First(&us).Error; err != nil {
		return nil, err
	}
	// 3. 不对外展示密码
	us.Password = ""
	return us, nil
}

func (u *UserServiceImpl) UpdateUser(ctx context.Context, req *user.UpdateRequest) (*user.User, error) {
	// 1. 先检验是否合法
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 填充更新字段
	us := user.NewUser(nil)
	if err := u.db.WithContext(ctx).Model(&user.User{}).Where("id=?", req.ID).First(&us).Error; err != nil {
		return nil, err
	}
	us.Role = req.Role
	if req.Password != "" {
		us.Password = req.Password
		if err := us.HashPassword(); err != nil {
			return nil, err
		}
	}
	if req.Label != nil {
		us.Label = req.Label
	}

	// 3. 保持到数据库
	if err := u.db.WithContext(ctx).Model(&user.User{}).Where("id=?", req.ID).Save(&us).Error; err != nil {
		return nil, err
	}

	us.Password = ""
	return us, nil
}

func (u *UserServiceImpl) DeleteUser(ctx context.Context, req *user.DeleteRequest) (*user.User, error) {
	// 1. 校验字段
	if err := req.Validate(); err != nil {
		return nil, err
	}

	// 2. 删除.
	// TODO: 这里存在并发删除失败问题
	us := user.NewUser(nil)
	if err := u.db.WithContext(ctx).Model(&user.User{}).Where("id=?", req.ID).First(&us).Error; err != nil {
		return nil, err
	}
	if err := u.db.WithContext(ctx).Where("id=?", req.ID).Delete(&us).Error; err != nil {
		return nil, err
	}
	us.Password = ""
	return us, nil
}

func (u *UserServiceImpl) CheckUserPassword(ctx context.Context, req *user.CheckRequest) (*user.User, error) {
	// 1. 校验字段
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 查询
	var us user.User
	if err := u.db.WithContext(ctx).Model(&user.User{}).Where("username=?", req.Username).First(&us).Error; err != nil {
		return nil, err
	}
	// 3. 对比密码是否正确
	if err := req.DiffPassword(us.Password); err != nil {
		return nil, err
	}

	us.Password = ""
	return &us, nil
}

// 向Ioc注册使用，初始化对象
func (u *UserServiceImpl) Init() error {
	u.db = conf.GetDB()
	return nil
}

// 向Ioc注册使用, 从ioc中销毁对象
func (u *UserServiceImpl) Destory() error {
	return nil
}
