package user

import (
	"encoding/json"
	"time"

	"gorm.io/gorm"
)

func NewUser(req *CreateRequest) *User {
	return &User{
		CreateRequest: req,
		CreatedAt:     time.Now().Unix(),
	}
}

type User struct {
	ID        int            `gorm:"column:id;type:bigint;primaryKey;autoIncrement" json:"id"`
	CreatedAt int64          `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt int64          `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
	*CreateRequest
}

func (u *User) String() string {
	b, _ := json.Marshal(u)
	return string(b)
}
