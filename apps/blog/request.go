package blog

import (
	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
)

var validate = validator.New()

//////////////////////////////////////////////////////////

func NewCreateRequest() *CreateRequest {
	return &CreateRequest{}
}

// 用户创建博客
type CreateRequest struct {
	Title    string            `json:"title" validate:"required" gorm:"column:title;type:varchar;size:255"`
	Author   string            `json:"author" validate:"required" gorm:"column:author;type:varchar;size:255"`
	CreateBy string            `json:"create_by" validate:"-" gorm:"column:create_by;type:varchar;size:255"`
	Summary  string            `json:"summary,omitempty" validate:"omitempty" gorm:"column:summary;type:varchar;size:255"`
	Content  string            `json:"content" validate:"required" gorm:"column:content;type:text"`
	Tags     map[string]string `json:"tags,omitempty" validate:"omitempty" gorm:"column:tags;serializer:json"`
}

// 校验参数是否合法
func (c *CreateRequest) Validate() error {
	// TODO: CreateBy 必须是登录的用户，否则报错
	if err := validate.Struct(c); err != nil {
		logrus.Warnf("valiate CreateRequest struct failed,obj:%v,err:%s", c, err)
		return err
	}
	return nil
}

/////////////////////////////////////////////////////////

func NewChangeStatusRequest() *ChangeStatusRequest {
	return &ChangeStatusRequest{
		Status: STATUS_DRAFT,
	}
}

// 用户修改博客状态
type ChangeStatusRequest struct {
	Status       Status `json:"status" validate:"omitempty" gorm:"column:status;type:tinyint"`
	Published_at int64  `json:"published_at,omitempty" validate:"-" gorm:"column:published_at;type:int"`
}

// 校验参数是否合法
func (c *ChangeStatusRequest) Validate() error {
	if err := validate.Struct(c); err != nil {
		logrus.Warnf("valiate ChangeStatusRequest struct failed,obj:%v,err:%s", c, err)
		return err
	}
	return nil
}

/////////////////////////////////////////////////////////

func NewAuditRequest() *AuditRequest {
	return &AuditRequest{
		IsAuditPass: AUDIT_PASS,
	}
}

// 管理员审核博客，默认审核通过
type AuditRequest struct {
	IsAuditPass Audit `json:"is_audit_pass" validate:"-" gorm:"column:is_audit_pass;type:bool"`
	AuditAt     int   `json:"audit_at,omitempty" validate:"-" gorm:"column:audit_at;type:int"`
}

// 校验参数是否合法
func (a *AuditRequest) Validate() error {
	if err := validate.Struct(a); err != nil {
		logrus.Warnf("valiate AuditRequest struct failed,obj:%v,err:%s", a, err)
		return err
	}
	return nil
}

/////////////////////////////////////////////////////////

func NewQueryRequest(title string) *QueryRequest {
	return &QueryRequest{
		Title: title,
	}
}

// 用户查询博客的请求
type QueryRequest struct {
	Title    string `json:"title" validate:"required"`
	PageSize int    `validate:"omitempty,gte=1" json:"page_size"`
	PageNum  int    `validate:"omitempty,gte=1" json:"page_num"`
}

// 校验参数是否合法
func (q *QueryRequest) Validate() error {
	switch {
	case q.PageSize <= 0:
		q.PageSize = 10
	case q.PageSize > 100:
		q.PageSize = 100
	}

	if q.PageNum <= 0 {
		q.PageNum = 1
	}

	if err := validate.Struct(q); err != nil {
		logrus.Warnf("valiate QueryRequest struct failed,obj:%v,err:%s", q, err)
		return err
	}
	return nil
}

func (q *QueryRequest) Offset() int {
	return (q.PageNum - 1) * q.PageSize
}

func (q *QueryRequest) Limit() int {
	return q.PageSize
}

func NewSet() *Set {
	return &Set{
		Items: make([]*Blog, 0, 10), // 因为默认最小为10
	}
}

// 博客集合
type Set struct {
	Count int     `json:"count"`
	Items []*Blog `json:"items"`
}

/////////////////////////////////////////////////////////

func NewDescribeRequest(id int) *DescribeRequest {
	return &DescribeRequest{
		ID: id,
	}
}

// 查询具体博客详情
type DescribeRequest struct {
	ID int `json:"id" validate:"required,gte=1"`
}

// 校验参数是否合法
func (d *DescribeRequest) Validate() error {
	if err := validate.Struct(d); err != nil {
		logrus.Warnf("valiate DescribeRequest struct failed,obj:%v,err:%s", d, err)
		return err
	}
	return nil
}

// /////////////////////////////////////////////////////////////////

func NewUpdateRequest(id int, model Update) *UpdateRequest {
	return &UpdateRequest{
		ID:          id,
		UpdateModel: model,
	}
}

// 更新文章全部信息
type UpdateRequest struct {
	ID          int    `json:"id" validate:"required"`
	UpdateModel Update `json:"update_model"`
	CreateRequest
}

// 校验参数是否合法
func (u *UpdateRequest) Validate() error {
	// TODO: CreateBy 必须是登录的用户，否则报错
	if err := validate.Struct(u); err != nil {
		logrus.Warnf("valiate UpdateRequest struct failed,obj:%v,err:%s", u, err)
		return err
	}
	return nil
}

// ///////////////////////////////////////////////////////

func NewDeleteRequest(id int) *DeleteRequest {
	return &DeleteRequest{
		ID: id,
	}
}

// 删除博客
type DeleteRequest struct {
	ID int `json:"id" validate:"required"`
}

// 校验参数是否合法
func (d *DeleteRequest) Validate() error {
	// TODO: CreateBy 必须是登录的用户，否则报错
	if err := validate.Struct(d); err != nil {
		logrus.Warnf("valiate DeleteRequest struct failed,obj:%v,err:%s", d, err)
		return err
	}
	return nil
}
