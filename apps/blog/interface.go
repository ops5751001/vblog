package blog

import "context"

type Service interface {
	CreateBlog(ctx context.Context, req *CreateRequest) (*Blog, error)
	QueryBlogs(ctx context.Context, req *QueryRequest) (*Set, error)
	DescribeBlog(ctx context.Context, req *DescribeRequest) (*Blog, error)
	UpdateBlog(ctx context.Context, req *UpdateRequest) (*Blog, error)
	DeleteBlog(ctx context.Context, req *DeleteRequest) (*Blog, error)
}
