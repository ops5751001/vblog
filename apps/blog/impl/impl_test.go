package impl_test

import (
	"context"
	"testing"

	"gitlab.com/ops5751001/vblog/apps/blog"
	blog_impl "gitlab.com/ops5751001/vblog/apps/blog/impl"
	conf "gitlab.com/ops5751001/vblog/config"
)

var bgi *blog_impl.BlogServiceImpl
var ctx context.Context

func init() {
	conf.ReadConfigFromEnv()
	conf.Init()
	bgi = blog_impl.NewBlogServiceImpl()
}

func TestCreateBlog(t *testing.T) {
	var req = blog.NewCreateRequest()
	req.Author = "张三"
	req.Content = "Go官网下载地址：https://golang.org/dl/\nGo官方镜像站：https://golang.google.cn/dl/"
	req.Summary = "Golang开发环境配置和简单demo示例"
	req.Title = "Golang环境搭建"
	req.Tags = map[string]string{"lang": "golang", "level": "basic"}

	bg, err := bgi.CreateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(bg)
}

func TestQueryBlogs(t *testing.T) {
	var req = blog.NewQueryRequest("Golang环境搭建")
	set, err := bgi.QueryBlogs(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestDescribeBlog(t *testing.T) {
	var req = blog.NewDescribeRequest(1)
	bg, err := bgi.DescribeBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(bg)
}

func TestUpdateBlogPut(t *testing.T) {
	var req = blog.NewUpdateRequest(1, blog.UPDATE_FULL)
	req.Content = `GOPATH是一个环境变量，用来表明你写的go项目的存放路径。`
	req.Author = "张三"
	req.Title = "Golang环境搭建"
	req.Tags = map[string]string{}

	bg, err := bgi.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(bg)
}

func TestUpdateBlogPatch(t *testing.T) {
	var req = blog.NewUpdateRequest(1, blog.UPDATE_PATCH)
	req.Content = `GOPATH是一个环境变量，用来表明你写的go项目的存放路径。`
	req.Author = "张三"
	req.Title = "Golang环境搭建"

	bg, err := bgi.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(bg)
}

func TestDeleteBlog(t *testing.T) {
	var req = blog.NewDeleteRequest(1)
	bg, err := bgi.DeleteBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(bg)
}
