package impl

import (
	"context"
	"fmt"
	"time"

	"dario.cat/mergo"
	"gitlab.com/ops5751001/vblog/apps/blog"
	conf "gitlab.com/ops5751001/vblog/config"
	"gitlab.com/ops5751001/vblog/ioc"
	"gorm.io/gorm"
)

// 强制约束
var _ blog.Service = (*BlogServiceImpl)(nil)
var _ ioc.Object = (*BlogServiceImpl)(nil)

func init() {
	ioc.Controller().Register(blog.APPNAME, &BlogServiceImpl{})
}

func NewBlogServiceImpl() *BlogServiceImpl {
	return &BlogServiceImpl{
		db: conf.GetDB(),
	}
}

type BlogServiceImpl struct {
	db *gorm.DB
}

// 创建博客
func (b *BlogServiceImpl) CreateBlog(ctx context.Context, req *blog.CreateRequest) (*blog.Blog, error) {
	// 1. 校验请求字段
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 构造Blog结构体
	newBlog := blog.NewBlog(req)
	newBlog.AuditAt = int(time.Now().Unix())
	// 3. 写入数据库
	if err := b.db.WithContext(ctx).Model(&blog.Blog{}).Create(&newBlog).Error; err != nil {
		return nil, err
	}
	return newBlog, nil
}

// 查询博客列表
func (b *BlogServiceImpl) QueryBlogs(ctx context.Context, req *blog.QueryRequest) (*blog.Set, error) {
	// 1. 校验字段合法性
	if err := req.Validate(); err != nil {
		return nil, err
	}

	// 2. 创建集合接收响应
	set := blog.NewSet()
	var count int64

	// 3. 查询文章列表
	// TODO: 这里需要增加判断的，如果是当前用户的文章，则需要将所有文章展示出来
	// TODO: 这里只应该展示部分字段，最好使用单独的结构体接收，而不是使用 blog.Blog
	// 3.1. 查询总数
	db := b.db.WithContext(ctx).Model(&blog.Blog{}).Where("status = ? and title like ?", blog.STATUS_PUBLIC, "%"+req.Title+"%")
	if err := db.Count(&count).Error; err != nil {
		return nil, err
	}
	set.Count = int(count)
	// 3.2. 将指定页面的文章列表取出
	if err := db.Offset(req.Offset()).Limit(req.Limit()).Find(&set.Items).Error; err != nil {
		return nil, err
	}

	// 4. 为了减少流量传输，应该不发送文章内容，不然会导致传输的内容过大
	for _, v := range set.Items {
		v.Content = ""
	}

	return set, nil
}

// 查看博客详情
func (b *BlogServiceImpl) DescribeBlog(ctx context.Context, req *blog.DescribeRequest) (*blog.Blog, error) {
	// 1. 校验字段
	if err := req.Validate(); err != nil {
		return nil, err
	}
	// 2. 查询博客
	var res blog.Blog
	if err := b.db.WithContext(ctx).Model(&blog.Blog{}).Where("id = ?", req.ID).First(&res).Error; err != nil {
		return nil, err
	}
	
	return &res, nil
}

// 更新博客
func (b *BlogServiceImpl) UpdateBlog(ctx context.Context, req *blog.UpdateRequest) (*blog.Blog, error) {
	// 1. 校验字段
	if err := req.Validate();err != nil {
		return nil, err
	}

	// 2. 查找具体博客
	existBlog , err := b.DescribeBlog(ctx, blog.NewDescribeRequest(req.ID))
	if err != nil {
		return nil, err
	}
	
	// 区分不同的更新类型
	switch req.UpdateModel {
	case blog.UPDATE_PATCH:
		if err := mergo.Map(existBlog.CreateRequest, &req.CreateRequest, mergo.WithOverride); err != nil {
			return nil, err
		}
	case blog.UPDATE_FULL:
		existBlog.CreateRequest = &req.CreateRequest
	default:
		return nil, fmt.Errorf("unsupport %v update model", req.UpdateModel)
	}
	err = b.db.WithContext(ctx).Model(&blog.Blog{}).Where("id=?",existBlog.ID).Save(existBlog).Error

	return existBlog, err
}

// 删除博客
func (b *BlogServiceImpl) DeleteBlog(ctx context.Context, req *blog.DeleteRequest) (*blog.Blog, error) {
	// 1. 校验字段
	if err := req.Validate();err != nil {
		return nil, err
	}

	// 2. 查找博客
	var res blog.Blog
	if err := b.db.WithContext(ctx).Model(&blog.Blog{}).Where("id = ?", req.ID).First(&res).Error; err != nil {
		return nil, err
	}

	// 3. 删除博客
	if err := b.db.WithContext(ctx).Where("id = ?", req.ID).Delete(&blog.Blog{}).Error; err != nil {
		return nil, err
	}
	return &res, nil
}

// 向Ioc注册使用，初始化对象
func (b *BlogServiceImpl) Init() error {
	return nil
}

// 向Ioc注册使用, 从ioc中销毁对象
func (b *BlogServiceImpl) Destory() error {
	return nil
}
