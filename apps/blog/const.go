package blog

type Update int

type Status int

type Audit int

const (
	APPNAME              = "blog"
	UPDATE_FULL   Update = 0 // 全量更新
	UPDATE_PATCH  Update = 1 // 补丁更新
	STATUS_DRAFT  Status = 0 // 草稿状态
	STATUS_PUBLIC Status = 1 // 发布状态
	AUDIT_PASS    Audit  = 0 // 审核通过
	AUDIT_DENY    Audit  = 1 // 审核拒绝
)
