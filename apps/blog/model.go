package blog

import (
	"encoding/json"

	"gorm.io/gorm"
)

func NewBlog(req *CreateRequest) *Blog {
	return &Blog{
		CreateRequest:       req,
		ChangeStatusRequest: NewChangeStatusRequest(),
		AuditRequest:        NewAuditRequest(),
	}
}

type Blog struct {
	ID        int            `gorm:"column:id;type:bigint;primaryKey;autoIncrement" json:"id"`
	CreatedAt int64          `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt int64          `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
	*CreateRequest
	*ChangeStatusRequest
	*AuditRequest
}

func (b *Blog) String() string {
	j, _ := json.Marshal(b)
	return string(j)
}
