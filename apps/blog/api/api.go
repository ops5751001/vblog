package api

import (
	"io"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/ops5751001/vblog/apps/blog"
	"gitlab.com/ops5751001/vblog/ioc"
	"gitlab.com/ops5751001/vblog/middleware"
	"gitlab.com/ops5751001/vblog/response"
)

// 需要实现以下接口
var _ ioc.GinRouter = (*BlogApiHandler)(nil)
var _ ioc.Object = (*BlogApiHandler)(nil)

// IOC 注册
func init() {
	ioc.ApiHandler().Register(blog.APPNAME, &BlogApiHandler{})
}

func NewBlogApiHandler(svc blog.Service) *BlogApiHandler {
	return &BlogApiHandler{
		svc: svc,
	}
}

type BlogApiHandler struct {
	svc blog.Service
}

func (b *BlogApiHandler) RegisterGin(r gin.IRouter) {
	rg := r.Group(blog.APPNAME)
	rg.GET("/", b.Query)
	rg.GET("/:id", b.Describe)

	rg.Use(middleware.Auth)
	rg.POST("/", b.Create)
	rg.PUT("/:id", b.Update)
	rg.PATCH("/:id", b.Update)
	rg.DELETE("/:id", b.Delete)
}

func (b *BlogApiHandler) Create(c *gin.Context) {
	// 1. 解析请求
	var req *blog.CreateRequest
	if err := c.BindJSON(&req); err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		body, _ := io.ReadAll(c.Request.Body)
		logrus.Warnf("bind json failed,obj:%v,err:%s", body, err)
		return
	}
	// 2. 处理业务逻辑
	bg, err := b.svc.CreateBlog(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4012, err.Error())
		logrus.Warnf("create blog failed,obj:%v,err:%s", req, err.Error())
		return
	}
	// 3. 请求OK，返回正确的结果
	response.SendOK(c, http.StatusOK, 2000, bg)
}

func (b *BlogApiHandler) Query(c *gin.Context) {
	// 1. 解析请求
	var req *blog.QueryRequest
	if err := c.BindJSON(&req); err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		body, _ := io.ReadAll(c.Request.Body)
		logrus.Warnf("bind json failed,obj:%v,error:%s", body, err.Error())
		return
	}
	// 2. 处理业务逻辑
	bg, err := b.svc.QueryBlogs(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4012, err.Error())
		logrus.Warnf("query blog failed,obj:%v,err:%s", req, err.Error())
		return
	}
	// 3. 正确的请求
	response.SendOK(c, http.StatusOK, 2000, bg)

}

func (b *BlogApiHandler) Describe(c *gin.Context) {
	// 1. 解析请求
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		logrus.Warnf("get id from request faield,id:%s,err:%s", c.Param("id"), err.Error())
		return
	}
	var req = blog.NewDescribeRequest(id)
	// 2. 处理业务逻辑
	bg, err := b.svc.DescribeBlog(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4012, err.Error())
		logrus.Warnf("describe blog failed,id:%d,err:%s", id, err.Error())
		return
	}

	response.SendOK(c, http.StatusOK, 2000, bg)
}

func (b *BlogApiHandler) Update(c *gin.Context) {
	// 1. 解析请求
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		logrus.Warnf("get id from request faield,id:%s,err:%s", c.Param("id"), err.Error())
		return
	}
	var req *blog.UpdateRequest
	if c.Request.Method == http.MethodPut {
		req = blog.NewUpdateRequest(id, blog.UPDATE_FULL)
	} else {
		req = blog.NewUpdateRequest(id, blog.UPDATE_PATCH)
	}

	if err := c.BindJSON(req); err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		body, _ := io.ReadAll(c.Request.Body)
		logrus.Warnf("bind json failed,obj:%v,err:%s", body, err)
		return
	}
	// 2. 处理业务逻辑
	bg, err := b.svc.UpdateBlog(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4012, err.Error())
		logrus.Warnf("update blog failed,obj:%v,err:%s", req, err.Error())
		return
	}

	response.SendOK(c, http.StatusOK, 2000, bg)
}

func (b *BlogApiHandler) Delete(c *gin.Context) {
	// 1. 解析请求
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4011, err.Error())
		logrus.Warnf("get id from request faield,id:%s,err:%s", c.Param("id"), err.Error())
		return
	}
	var req = blog.NewDeleteRequest(id)
	// 2. 处理业务逻辑
	bg, err := b.svc.DeleteBlog(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusBadRequest, 4012, err.Error())
		logrus.Warnf("delete blog failed,id:%d,err:%s", id, err)
		return
	}
	response.SendOK(c, http.StatusOK,2000, bg)
}


func (b *BlogApiHandler) Init() error {
	return nil
}

func (b *BlogApiHandler) Destory() error {
	return nil
}
