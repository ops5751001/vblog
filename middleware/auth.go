package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ops5751001/vblog/apps/token"
	"gitlab.com/ops5751001/vblog/ioc"
	"gitlab.com/ops5751001/vblog/response"
)

func Auth(c *gin.Context) {
	// 从http请求获取token
	tkFormHTTP, err := token.NewRevokeRequestFromHttp(c.Request)
	if err != nil {
		response.SendFailed(c, http.StatusUnauthorized, 4010, err.Error())
		return
	}
	// 校验token
	var req = token.NewValidateRequest(tkFormHTTP.AccessToken, tkFormHTTP.RefreshToken)
	tk, err := ioc.Controller().GetObj(token.APPNAME).(token.Service).Validate(c, req)
	if err != nil {
		response.SendFailed(c, http.StatusUnauthorized, 4011, err.Error())
		return
	}
	// 校验成功后将结构体塞入上下文
	c.Set(token.TOKEN_MIDDLEWARE_KEY, tk)
}
