package conf_test

import (
	"testing"

	"gitlab.com/ops5751001/vblog/config"
)

func TestReadConfigFromToml(t *testing.T) {
	conf.ReadConfigFromToml("../scripts/etc/application.toml")
	conf.GetDB()
	t.Log(conf.GlobalConfig)
}

func TestReadConfigFromYaml(t *testing.T) {
	conf.ReadConfigFromYaml("../scripts/etc/application.yaml")
	conf.GetDB()
	t.Log(conf.GlobalConfig)
}

func TestReadConfigFromEnv(t *testing.T) {
	conf.ReadConfigFromEnv()
	conf.GetDB()
	t.Log(conf.GlobalConfig)
}