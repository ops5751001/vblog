package conf

import (
	"encoding/json"
	"fmt"

	"gitlab.com/ops5751001/vblog/ioc"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var _ ioc.Object = (*Config)(nil)

func init() {
	ioc.Config().Register(APPNAME, GlobalConfig)
}

// 设置配置的默认值
func NewDefaultConfig() *Config {
	mysql := &MySQL{
		Host:     "127.0.0.1",
		Port:     3306,
		Username: "root",
		Password: "123456",
		Database: "vblog",
		Debug:    false,
	}
	token := &Token{
		AccessTokenExpiredAt:  60 * 60 * 2,
		RefreshTokenExpiredAt: 60 * 60 * 24,
		MemberTokenRatio:      7,
	}
	basic := &Basic{
		ListenIP:   "0.0.0.0",
		ListenPort: 8081,
		DomainName: "localhost",
	}
	return &Config{
		MySQL: mysql,
		Token: token,
		Basic: basic,
	}
}

type Config struct {
	MySQL *MySQL `toml:"mysql" json:"mysql" yaml:"mysql"`
	Token *Token `toml:"token" json:"token" yaml:"token"`
	Basic *Basic `toml:"basic" json:"basic" yaml:"basic"`
}

func (c *Config) String() string {
	content, _ := json.Marshal(c)
	return string(content)
}

// 向Ioc注册使用，初始化对象
func (c *Config) Init() error {
	if err := c.MySQL.connect(); err != nil {
		return err
	}
	if err := c.Token.validate(); err != nil {
		return err
	}
	return nil
}

// 向Ioc注册使用, 从ioc中销毁对象
func (c *Config) Destory() error {
	return nil
}

type MySQL struct {
	Host     string   `json:"host" yaml:"host" toml:"host" env:"HOST"`
	Port     int      `json:"port" yaml:"port" toml:"port" env:"PORT"`
	Username string   `json:"username" yaml:"username" toml:"username" env:"USERNAME"`
	Password string   `json:"password" yaml:"password" toml:"password" env:"PASSWORD"`
	Database string   `json:"database" yaml:"database" toml:"database" env:"DATABASE"`
	Debug    bool     `json:"debug" yaml:"debug" toml:"debug" env:"DEBUG"`
	db       *gorm.DB `json:"-" yaml:"-" toml:"-" env:"-"`
}

// 测试连接MySQL
func (m *MySQL) connect() error {
	// dsn := "root:123456@tcp(127.0.0.1:3306)/example?charset=utf8mb4&parseTime=true"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=true",
		m.Username, m.Password, m.Host, m.Port, m.Database)
	var err error
	m.db, err = gorm.Open(mysql.Open(dsn))
	if err != nil {
		return err
	}
	if m.Debug {
		m.db.Logger = logger.Default.LogMode(logger.Info)
	}
	return nil
}

func (m *MySQL) DB() *gorm.DB {
	return m.db
}

type Token struct {
	AccessTokenExpiredAt  int64 `json:"access_token_expired_at" yaml:"access_token_expired_at" env:"ACCESS_TOKEN_EXPIRED_AT" toml:"access_token_expired_at"`
	RefreshTokenExpiredAt int64 `json:"refresh_token_expired_at" yaml:"refresh_token_expired_at" env:"REFRESH_TOKEN_EXPIRED_AT" toml:"refresh_token_expired_at"`
	// 延长几倍的时间
	MemberTokenRatio int64 `json:"member_token_ratio" yaml:"member_token_ratio" env:"MEMBER_TOKEN_RATIO" toml:"member_token_ratio"`
}

// 校验 Token 配置合法
func (t *Token) validate() error {
	if t.AccessTokenExpiredAt > t.RefreshTokenExpiredAt {
		return fmt.Errorf("access token expired must be less than refress token expired")
	}
	if t.MemberTokenRatio <= 0 {
		return fmt.Errorf("member token ratio must be great than zero")
	}
	return nil
}

type Basic struct {
	ListenIP   string `json:"listen_ip" yaml:"listen_ip" env:"LISTEN_IP" toml:"listen_ip"`
	ListenPort int    `json:"listen_port" yaml:"listen_port" env:"LISTEN_PORT" toml:"listen_port"`
	DomainName string `json:"domain_name" yaml:"domain_name" env:"DOMAIN_NAME" toml:"domian_name"`
	CookiePath string `json:"cookie_path" yaml:"cookie_path" env:"COOKIE_PATH" toml:"cookie_path"`
}
