package conf

import (
	"fmt"

	"gorm.io/gorm"
)

const APPNAME = "config"

// TODO: 这里应该单独定义函数用来取GlobalConfig的配置，这样GlobalConfig对象就不需要被外部引用
var GlobalConfig = NewDefaultConfig()

func Init() {
	GlobalConfig.Init()
}

func GetDB() *gorm.DB {
	return GlobalConfig.MySQL.db
}

func GetAccessTokenExpiredAt() int64 {
	return GlobalConfig.Token.AccessTokenExpiredAt
}

func GetRefreshTokenExpiredAt() int64 {
	return GlobalConfig.Token.RefreshTokenExpiredAt
}

func GetRefreshTokenExpiredAtWithRemeber() int64 {
	return GlobalConfig.Token.RefreshTokenExpiredAt * GlobalConfig.Token.MemberTokenRatio
}

func GetDomainName() string {
	return GlobalConfig.Basic.DomainName
}

func GetCookiesPath() string {
	return GlobalConfig.Basic.CookiePath
}

func GetListenAddress() string {
	return fmt.Sprintf("%s:%d", GlobalConfig.Basic.ListenIP, GlobalConfig.Basic.ListenPort)
}
