package conf

import (
	"os"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

func ReadConfigFromToml(f string) {
	if _, err := toml.DecodeFile(f, &GlobalConfig); err != nil {
		logrus.Panicf("decode %s faile,err:%s", f, err.Error())
	}
}

func ReadConfigFromYaml(f string) {
	content, err := os.ReadFile(f)
	if err != nil {
		logrus.Panicf("read config %s faile,err:%s", f, err.Error())
	}

	if err = yaml.Unmarshal(content, &GlobalConfig); err != nil {
		logrus.Panicf("yaml ummarshal config %s faile,err:%s", f, err.Error())
	}
}

func ReadConfigFromEnv() {
	if err := env.Parse(GlobalConfig); err != nil {
		logrus.Panicf("read config from env faile,err:%s",err.Error())
	}
}


