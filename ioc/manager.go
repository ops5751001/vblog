package ioc

import "github.com/gin-gonic/gin"

var GlobalNamespace = NewNamespace()

// 初始化
func Init() error {
	return GlobalNamespace.init()
}

// 销毁
func Destory() error {
	return GlobalNamespace.destory()
}

// 注册API
func ReginsterApiGinRouter(r gin.IRouter) {
	GlobalNamespace.reginsterApiGinRouter(r)
}

// 获取不同Conatiner
func Controller() *Container {
	return GlobalNamespace.get("controller")
}

func ApiHandler() *Container {
	return GlobalNamespace.get("apiHandler")
}

func Config() *Container {
	return GlobalNamespace.get("config")
}

func Default() *Container {
	return GlobalNamespace.get("default")
}