package ioc

import "github.com/gin-gonic/gin"

// ioc 管理对象的约束
type Object interface {
	Init() error
	Destory() error
}

// 具备gin路由注册功能的约束
type GinRouter interface {
	Object
	RegisterGin(gin.IRouter)
}
