package ioc_test

import (
	"testing"

	"gitlab.com/ops5751001/vblog/ioc"
)

type TestStruct struct{}

func (t *TestStruct) Init() error {
	return nil
}

func (t *TestStruct) Destory() error {
	return nil
}

func TestRegister(t *testing.T) {
	c := ioc.NewContainer()
	c.Register("testStuct", &TestStruct{})
	obj := c.GetObj("testStuct")
	if _, ok := obj.(*TestStruct); ! ok {
		t.Failed()
	}
}
