package ioc

import "github.com/gin-gonic/gin"

func NewContainer() *Container {
	return &Container{
		obj: map[string]Object{},
	}
}

// 容器，用来存储 ioc 对象
type Container struct {
	obj map[string]Object
}

// 注册ioc对象
func (c *Container) Register(k string, v Object) {
	c.obj[k] = v
}

// 获取ioc对象
func (c *Container) GetObj(k string) any {
	return c.obj[k]
}

func NewNamespace() *Namespace {
	return &Namespace{
		ns: map[string]*Container{
			"controller": NewContainer(),
			"apiHandler": NewContainer(),
			"default":    NewContainer(),
			"config":     NewContainer(),
		},
	}
}

// 将container封装为两层结构
type Namespace struct {
	ns map[string]*Container
}

// 逐个初始化
func (n *Namespace) init() error {
	// 优先初始化配置文件
	for _, v := range n.ns["config"].obj {
		if err := v.Init(); err != nil {
			return err
		}
	}
	for k, c := range n.ns {
		if k != "config" {
			for _, v := range c.obj {
				if err := v.Init(); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// 注册路由
func (n *Namespace) reginsterApiGinRouter(r gin.IRouter) {
	for _, c := range n.ns {
		for _, v := range c.obj {
			if api, ok := v.(GinRouter); ok {
				api.RegisterGin(r)
			}
		}
	}
}

// 逐个销毁.非致命错误，不应该返回
func (n *Namespace) destory() error {
	for _, c := range n.ns {
		for _, v := range c.obj {
			if err := v.Destory(); err != nil {
				return err
			}
		}
	}
	return nil
}

// 获取指定的ns
func (n *Namespace) get(k string) *Container {
	return n.ns[k]
}
