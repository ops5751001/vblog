package response

import (
	"github.com/gin-gonic/gin"
)

func newResponse(code int, data interface{}, msg string) *Response {
	return &Response{
		Code: code,
		Msg:  msg,
		Data: data,
	}
}

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg,omitempty"`
	Data interface{} `json:"data,omitempty"`
}

func SendOK(c *gin.Context, httpCode, svcCode int, data interface{}) {
	res := newResponse(svcCode,data,"")
	c.JSON(httpCode, res)
}

func SendFailed(c *gin.Context, httpCode, svcCode int, msg string) {
	res := newResponse(svcCode,"",msg)
	c.JSON(httpCode, res)
}

