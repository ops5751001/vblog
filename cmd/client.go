package cmd

import (
	"github.com/spf13/cobra"
)

var clientCmd = &cobra.Command{
	Use:   "client",
	Short: "执行客户端指令,比如创建user",
	// Run:   startClient,
}

var (
	clientConfigFile string
)

func init() {
	rootCmd.AddCommand(clientCmd)
}
