package cmd

import (
	"os"

	"github.com/spf13/cobra"

	_ "gitlab.com/ops5751001/vblog/config"
	_ "gitlab.com/ops5751001/vblog/apps"
)

// 根命令，定义了当前这个项目二进制文件命令行参数
var rootCmd = &cobra.Command{
	Use:     "vblog",
	Version: "v0.0.1-alpha",
	Short:   "个人博客项目",        // 短描述信息
	Long:    `个人博客项目 ......`, // 长描述信息
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

func Execute() { // 被 main.go 引用的函数
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
