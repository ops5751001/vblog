package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/ops5751001/vblog/apps/user"
	conf "gitlab.com/ops5751001/vblog/config"
	"gitlab.com/ops5751001/vblog/ioc"
	"github.com/sethvargo/go-password/password"
)

var userCmd = &cobra.Command{
	Use:   "user",
	Short: "增删改查用户",
	Run:   manmageUser,
}

var userConfig = &ClientUser{}

type ClientUser struct {
	Action     string
	Username   string
	Password   string
	Role       int8
	PageSize   int
	PageNumber int
}

func init() {
	clientCmd.AddCommand(userCmd)
	userCmd.Flags().StringVarP(&userConfig.Action, "action", "a", "create", "创建用户,支持create/delete/query")
	userCmd.Flags().StringVarP(&userConfig.Username, "username", "u", "", "指定用户名")
	userCmd.Flags().StringVarP(&userConfig.Password, "password", "p", "", "指定密码")
	userCmd.Flags().Int8VarP(&userConfig.Role, "role", "r", 0, "指定创建的用户角色")
	userCmd.Flags().IntVarP(&userConfig.PageSize, "size", "s", 20, "指定每页返回的数量")
	userCmd.Flags().IntVarP(&userConfig.PageNumber, "number", "n", 1, "指定页码")
	userCmd.Flags().StringVarP(&clientConfigFile, "config", "c", "scripts/etc/application.toml", "指定启动的配置文件路径")

	_ = clientCmd.MarkFlagRequired("action")
}

func manmageUser(cmd *cobra.Command, args []string) {
	// 初始化配置文件
	conf.ReadConfigFromToml(clientConfigFile)
	// 初始化Ioc组件
	ioc.Init()

	// 执行用户的增删改查
	switch userConfig.Action {
	case "create":
		createUser(cmd)
	case "delete":
		deleteUser(cmd)
	case "query":
		queryUser(cmd)
	default:
		cobra.CheckErr(fmt.Errorf("action must be one of [create,query,delete]"))
	}
}

func createUser(cmd *cobra.Command) {
	svc := ioc.Controller().GetObj(user.APPNAME).(user.Service)
	req := user.NewCreateRequest()
	req.Username = userConfig.Username
	if userConfig.Password == "" {
		p, _ := password.Generate(10,2,0, true,true)
		userConfig.Password = p
	}
	req.Password = userConfig.Password
	fmt.Println(req)
	_, err := svc.CreateUser(cmd.Context(), req)
	cobra.CheckErr(err)
}

/* 查询用户
[root@RockyLinux vblog]# go run main.go client user -a query
+----+----+----------+--------+---------------------------+---------------------------+
| no | id | username |  role  |          created          |          updated          |
+----+----+----------+--------+---------------------------+---------------------------+
| 1  | 6  |   lisi   | 管理员 | 2024-01-30 08:41:09 +0800 | 2024-01-30 08:41:09 +0800 |
| 2  | 7  |  wangwu  |  游客  | 2024-01-30 08:41:23 +0800 | 2024-01-30 08:41:23 +0800 |
| 3  | 8  |   tom    |  游客  | 2024-01-30 08:41:35 +0800 | 2024-01-30 08:41:35 +0800 |
| 4  | 11 | zhangsan |  游客  | 2024-02-24 09:56:16 +0800 | 2024-02-24 09:56:16 +0800 |
+----+----+----------+--------+---------------------------+---------------------------+
*/
func queryUser(cmd *cobra.Command) {
	svc := ioc.Controller().GetObj(user.APPNAME).(user.Service)
	// fmt.Println(svc)
	req := user.NewQueryRequest(userConfig.Username)
	req.PageSize = userConfig.PageSize
	req.PageNum = userConfig.PageNumber
	set, err := svc.QueryUsers(cmd.Context(), req)
	cobra.CheckErr(err)
	set.Print()
}

func deleteUser(cmd *cobra.Command) {

}
