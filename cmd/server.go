package cmd

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	conf "gitlab.com/ops5751001/vblog/config"
	"gitlab.com/ops5751001/vblog/ioc"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "启动vblog apiserver",
	Run:   startServer,
}

var (
	serverConfigFile string
	ServerApiGroup   string
)

func startServer(cmd *cobra.Command, args []string) {
	// 初始化配置文件
	conf.ReadConfigFromToml(serverConfigFile)

	// 初始化 gin 框架路由
	engine := gin.Default()
	rg := engine.Group(ServerApiGroup)

	// 初始化Ioc组件
	ioc.Init()
	ioc.ReginsterApiGinRouter(rg)

	// 开启监听
	if err := engine.Run(conf.GetListenAddress()); err != nil {
		panic(err)
	}
}

func init() {
	rootCmd.AddCommand(serverCmd)
	serverCmd.Flags().StringVarP(&serverConfigFile, "config", "c", "scripts/etc/application.toml", "指定启动的配置文件路径")
	serverCmd.Flags().StringVarP(&ServerApiGroup, "api", "a", "/vblog/api/v1", "指定API群组")
}
